import {from, fromEvent, interval, Observable, of, range, timer} from 'rxjs';
import {
  debounceTime, distinct, distinctUntilChanged,
  filter,
  find,
  findIndex,
  first,
  last,
  map,
  pluck,
  skip,
  skipUntil,
  skipWhile,
  take, takeUntil,
  takeWhile,
  tap
} from 'rxjs/operators';
import {fromPromise} from 'rxjs/internal-compatibility';

let observableTest = new Observable((observer: any) => {
  observer.next('Hello World!');

  setTimeout(() => {
    observer.next('Hello again (after 3 sec)');
    observer.error('Some error!');
  }, 3000);

  setTimeout(() => {
    observer.next('Hello again (after 1 sec)');
  }, 1000);

  setTimeout(() => {
    observer.next('Hello again (after 2 sec)');
    // observer.complete();
  }, 2000);

  observer.next('Bye!');
})

const inputElem = document.getElementById('input-elem');
const buttonElem = document.getElementById('button-elem');
const btnStream$ = fromEvent(buttonElem, 'click');
const coordinates$ = fromEvent(document, 'mousemove');
const types$ = of(5, 1, 2, 'string', [9,8,7,6], {a: 1, b: 2, c: 3});
const interval$ = interval(1000);
const timer$ = timer(4000, 500);
const range$ = range(0, 15);
const from$ = from([1,'string', {o: 'object'}, ['arr1', 'arr2']]);
const array$ = from([1,1,2,3,4,4,4,5,5,6,7,77,77,88,9,99]);
const set$ = from(new Set([1,2,3,'4','5', {id: 6}]));
const mapArray$ = from(new Map([[1,2], [3,4], [5,6]]));
const promise$ = fromPromise(delay(4000));
const map$ = interval(1000);
const of$ = of('hello', 'world', 'wfm');
const inputEvent$ = fromEvent(inputElem, 'keyup');
const firstLast$ = of(1,2,3,4,5);
const find$ = of<string|number>(1, 2, 3, 4, 5, 6, 'hello', 8, 9, 10);
const cars = [
  { name: 'Audi', price: 500},
  { name: 'BMW', price: 700},
  { name: 'Ford', price: 100},
  { name: 'Kalina', price: 50},
];

function logSubscribe(name: string) {
  return {
    next(x: any) {
      console.log(name, ': ', x)
    },
    error(err: any) {
      console.log('Error: ', err)
    },
    complete() {
      console.log(name, ': Completed!')
    },
  }
}

function delay(ms = 1000) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(ms);
    }, ms)
  });
}

observableTest.pipe(skip(1)).subscribe(logSubscribe('observableTest'));

fromEvent(inputElem, 'click')
  .pipe(map((e: any) => e.target.value))
  .subscribe(logSubscribe('observableInput'))

btnStream$.subscribe(logSubscribe('observableButton'))
coordinates$
  .pipe(
    map((e: MouseEvent) => {
      return {
        x: e.clientX,
        y: e.clientY,
      }
    })
  )
  .subscribe((e) => {
    document.querySelector('h2').innerHTML = `X: ${e.x}, Y: ${e.y}`
  });
types$.subscribe(logSubscribe('observableNumbers'));
interval$.pipe(take(5)).subscribe(logSubscribe('observableInterval'));
timer$.pipe(take(5)).subscribe(logSubscribe('observableTimer'));
range$.subscribe(logSubscribe('observableRange'));
from$.subscribe(logSubscribe('from'));
set$.subscribe(logSubscribe('fromSet'));
mapArray$.subscribe(logSubscribe('fromMap'));
promise$.subscribe(logSubscribe('fromPromise'));
map$.pipe(map(x => x * x), take(10)).subscribe(logSubscribe('map'));
of$.pipe(map(m => m.toUpperCase())).subscribe(logSubscribe('of'));
inputEvent$.pipe(
  // map((e: KeyboardEvent) => e.target.value),
  pluck('target', 'value'),
  map((v: string) => v.toUpperCase()),
  map(v => ({
    value: v,
    length: v.length
  }))
).subscribe(logSubscribe('input event'));
firstLast$.pipe(
  // first(),
  last()
).subscribe(logSubscribe('first-last'));
find$.pipe(
  // find(x => x === 'hello')
  // findIndex(x => x === 1) // выведет 0
  // take(3)
  // skip(2)
  // skipWhile(x => typeof x === 'number') // пропускать до тех пор пока x будет number
  // takeWhile(x => typeof x === 'number' ) // брать до тех пор пока x будет number
  // skipUntil(timer(3000)) // пропускать до тех пор пока не пройдёт 3 сек
  takeUntil(timer(3000)) // брать до тех пор пока не пройдёт 3 сек
).subscribe(logSubscribe('find-take-skip'));
range$.pipe(
  filter(x => x === 3)
).subscribe(logSubscribe('filter'));
inputEvent$.pipe(
  pluck('target', 'value')
)
  .subscribe(v => {
      from(cars).pipe(
        filter(car => car.name === v),
      ).subscribe(c => {
          document.querySelector('div')
            .innerHTML = `<h2>${c.name.toUpperCase()}</h2><h4>${c.price}</h4>`
        })
  });

inputEvent$.pipe(
  pluck('target', 'value'),
  distinct() // не эммитит значение пока оно не изменилось
  // debounceTime(1500) // задержка потока
).subscribe(logSubscribe('debounceTime'))

array$.pipe(
  distinctUntilChanged()
).subscribe(logSubscribe('array'))
